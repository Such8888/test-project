import { NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";
import {
  ModalModule,
  CollapseModule,
  BsDatepickerModule,
  TooltipModule,
  TimepickerModule,
} from "ngx-bootstrap";
import { AddModalComponent } from "./components/add-modal/add-modal.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { GridModule } from "@progress/kendo-angular-grid";
import { DeleteConfirmationModalComponent } from "./components/delete-confirmation-modal/delete-confirmation-modal.component";
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";
import { DeleteModalComponent } from "./components/delete-modal/delete-modal.component";
import { FormControlValidationMsgDirective } from "./directives/validators/validation-message.directive";
import { FormSubmitValidationMsgDirective } from "./directives/validators/submit-validation-msg.directive";
import { ValidationMsgService } from "./directives/validation-message.service";
import { DefaultImagePreviewDirective } from "./directives/default-image-preview/default-image-preview.directive";

@NgModule({
  declarations: [
    AddModalComponent,
    DeleteConfirmationModalComponent,
    DeleteModalComponent,

    DefaultImagePreviewDirective,
    FormControlValidationMsgDirective,
    FormSubmitValidationMsgDirective,
  ],
  imports: [
    CommonModule,
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    GridModule,
    DropDownsModule,
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    TooltipModule.forRoot(),
  ],
  exports: [
    AddModalComponent,
    FormsModule,
    ModalModule,
    ReactiveFormsModule,
    GridModule,
    CollapseModule,

    DeleteConfirmationModalComponent,
    DropDownsModule,
    BsDatepickerModule,
    TooltipModule,
    DeleteModalComponent,

    DefaultImagePreviewDirective,
    FormControlValidationMsgDirective,
    FormSubmitValidationMsgDirective,
  ],
  providers: [DatePipe, ValidationMsgService],
})
export class SharedModule {}
