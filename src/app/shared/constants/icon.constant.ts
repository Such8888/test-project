export const IconConst = {
  DASHBOARD: "fas fa-fw fa-tachometer-alt",


  BTN: {
    EDIT: "fa fa-edit",
    DELETE: "fa  fa-trash",
  },

  EXPORT_TO_PDF: "fas fa-file-pdf",
  EXPORT_TO_CSV: "fas fa-file-csv",
  PRINT: "fas fa-print",
  EMAIL: "fas fa-envelope",
};
