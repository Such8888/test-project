import { Component, OnInit } from "@angular/core";

import { IconConst } from "../../../../shared/constants/icon.constant";
import { environment } from "@env/environment.prod";

@Component({
  selector: "app-side-nav",
  templateUrl: "./side-nav.component.html",
  styleUrls: ["./side-nav.component.scss"],
})
export class SideNavComponent implements OnInit {
  defaultImagePath = environment.defaultImagePath;
  imageUrl = environment.defaultImageUrl;
  iconConst = IconConst;

  navItems = [
    {
      id: 1,
      displayName: "Dashboard",
      iconName: this.iconConst.DASHBOARD,
      route: "/dashboard",
    },
  ];

  constructor() {}

  ngOnInit() {}

  toogleSideNav() {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
  }
}
