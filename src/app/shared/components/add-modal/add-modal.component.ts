import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  TemplateRef,
} from "@angular/core";
import { BsModalService, BsModalRef } from "ngx-bootstrap";
import { ValidationMessageService } from "@app/shared/services/validation-message/validation-message.service";

@Component({
  selector: "crm-add-modal",
  templateUrl: "./add-modal.component.html",
  styleUrls: ["./add-modal.component.scss"],
})
export class AddModalComponent implements OnInit {
  @Input() type: string = "add";
  @Input() buttonName: string;
  @Input() modalTitle: string;
  @Input() formValid: boolean;

  @Output() onSubmit = new EventEmitter();

  modalRef: BsModalRef;

  // modal config to unhide modal when clicked outside
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
  };

  constructor(
    private modalService: BsModalService,
    private validationMessageService: ValidationMessageService
  ) {}

  ngOnInit() {}

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }

  onSave(): void {
    this.onSubmit.emit(true);

    if (this.formValid) {
      // to hide the red border on form field
      this.validationMessageService.formSubmitted = false;
      this.modalRef.hide();
    }
  }

  onCancel(): void {
    // to hide the red border on form field
    this.validationMessageService.formSubmitted = false;
    this.modalRef.hide();
  }
}
