import { ToastrMessageService } from './../../../../shared/services/toastr-message/toastr-message.service';
import { Component, OnInit } from "@angular/core";

import { Router } from "@angular/router";

import * as $ from "jquery";
import { environment } from "@env/environment";

@Component({
  selector: "app-nav-bar",
  templateUrl: "./nav-bar.component.html",
  styleUrls: ["./nav-bar.component.scss"],
})
export class NavBarComponent implements OnInit {
  defaultImagePath = environment.defaultImagePath;

  userInfo: any;
  imageUrl: any;
  userName: any;

  constructor(
    private toastrMessageService: ToastrMessageService,
    private router: Router,
  ) {}

  ngOnInit() {
  }

  navigateToDashboard() {
    this.router.navigate(["/dashboard"]);
  }



  logOut(): void {

    this.router.navigate(["/login"])
    this.toastrMessageService.showSuccess("You are succesfully logged out.")
  }
}
