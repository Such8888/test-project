import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LayoutModule } from "./layout/layout.module";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LayoutModule,
    CommonModule,
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  exports: [LayoutModule],
})
export class CoreModule {}
