import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
} from "@angular/forms";
import { ValidationMessageService } from "@app/shared/services/validation-message/validation-message.service";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";

@Component({
  selector: "crm-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private toastrMessageService: ToastrMessageService
  ) {}

  ngOnInit() {
    this.buildLoginForm();
  }

  buildLoginForm() {
    this.loginForm = this.formBuilder.group({
      username: ["", Validators.required],
      password: ["", Validators.required],
    });
  }

  onLogin() {
   if(this.loginForm.value.username == "sachet" && this.loginForm.value.password == "sachet") {
    this.router.navigate(["/dashboard"])
    this.toastrMessageService.showSuccess("You are succesfully logged in.")
   } else {
    this.toastrMessageService.showError("Username and password do not match.")
   }
  }

  passwordType = "Password";
  viewPassword(): void {
    if (this.passwordType == "Password") {
      this.passwordType = "text";
    } else {
      this.passwordType = "Password";
    }
  }

  //Form Controls
  get username(): AbstractControl {
    return this.loginForm.get("username");
  }

  get password(): AbstractControl {
    return this.loginForm.get("password");
  }
}
