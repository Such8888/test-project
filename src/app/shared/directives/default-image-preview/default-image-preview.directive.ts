import {
  Directive,
  Input,
  HostListener,
  HostBinding,
  OnInit,
} from "@angular/core";
@Directive({
  selector: "img",
})
export class DefaultImagePreviewDirective implements OnInit {
  @Input()
  default: string = "./assets/images/no-image-available.png";

  @HostListener("error")
  onError() {
    this.src = this.default;
  }

  @HostBinding("src")
  @Input()
  src: string;

  ngOnInit() {}
}
