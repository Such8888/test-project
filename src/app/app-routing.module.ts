import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AdminPanelComponent } from "./core/layout/admin-panel/admin-panel.component";

const routes: Routes = [
  {
    path: "login",
    loadChildren: "@modules/auth/login/login.module#LoginModule",
  },
  {
    path: "",
    component: AdminPanelComponent,
    // canActivate: [AuthGuard],
    children: [
      {
        path: "",
        redirectTo: "dashboard",
        pathMatch: "full",
      },
      {
        path: "dashboard",
        loadChildren: "@modules/dashboard/dashboard.module#DashboardModule",
        data: {
          breadcrumb: "Dashboard",
        },
      },

    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      // to emit router events when we try to navigate to the same URL
      onSameUrlNavigation: "reload",
    }),
  ],

  exports: [RouterModule],
})
export class AppRoutingModule {}
