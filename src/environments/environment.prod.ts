export const environment = {
  production: true,
  baseIP: "https://crm.bentray.work/",
  apiPrefix: "api/web/v1/",

  defaultImageUrl: "./assets/images/",
  defaultImagePath: "./assets/dist/img",
};
