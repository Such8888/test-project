import { ToastrMessageService } from './../../../shared/services/toastr-message/toastr-message.service';
import { Component, OnInit } from "@angular/core";
import { FooterComponent } from "./footer/footer.component";
import { Subscription } from "rxjs";
import { CookieService } from "ngx-cookie-service";

import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";

@Component({
  selector: "app-admin-panel",
  templateUrl: "./admin-panel.component.html",
  styleUrls: ["./admin-panel.component.scss"],
})
export class AdminPanelComponent implements OnInit {
  entryComponent = FooterComponent;
  pageTitle: any;
  subscription: Subscription;
  constructor(
    private location: Location,
    private cookieService: CookieService,
    private toastrMessageService: ToastrMessageService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        let child = this.activatedRoute.firstChild;
        this.pageTitle = child.snapshot.data["breadcrumb"];


      }
    });
  }

  ngOnInit() {}

  logout(): void {
    this.router.navigate(["/login"]);
    this.toastrMessageService.showError("You are succesfully logged out.")
  }

  goBack(): void {
    this.location.back();
  }
}
